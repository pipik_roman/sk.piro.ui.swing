package sk.piro.ui.swing
import java.awt.Window
import java.awt.Toolkit
import java.awt.Point
import java.awt.Dimension
import java.lang.Math

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 *
 */
object SwingUtils {
  /**
   * Center window on screen and returns upper left corner of window
   */
  def center(window : Window) : Unit = {
    val windowSize = window.getSize
    val screenSize = SwingUtils.screenSize(window)
    val width = (screenSize._1 / 2) - (windowSize.width / 2)
    val height = (screenSize._2 / 2) - (windowSize.height / 2)
    window.setLocation(new Point(width, height))
  }

  /**
   * If window is larger than screen it is resized to screen size
   */
  def crop(window : Window) : Boolean = {
    val screenSize = SwingUtils.screenSize(window)
    val windowSize = window.getSize
    windowSize.width = windowSize.width.min(screenSize._1)
    windowSize.height = windowSize.height.min(screenSize._2)
    window.setSize(windowSize.width, windowSize.height)
    windowSize.width == screenSize._1 && windowSize.height == screenSize._2
  }

  /**
   * Returns screen size for specified window (supporting multiple displays). If no window is specified default screen size is returned
   */
  def screenSize(window : Window = null) : (Int, Int) = {
    var dim : Dimension = null
    if (window == null) {
      dim = Toolkit.getDefaultToolkit.getScreenSize
    } else {
      dim = window.getGraphicsConfiguration.getBounds.getSize
    }
    (dim.width, dim.height)
  }

  def packAndCrop(window : Window) : Unit = {
    window.pack
    crop(window)
  }

  def packCropAndCenter(window : Window) : Unit = {
    window.pack
    crop(window)
    center(window)
  }
}